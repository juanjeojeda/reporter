"""Main reporter definition."""
import os

from cki_lib.logger import get_logger

from . import settings


LOGGER = get_logger('cki.reporter')


# pylint: disable=unused-argument
def process_message(routing_key, payload):
    """Process the webhook message."""
    LOGGER.info('TODO -- process a message (payload) here')


def main():
    """Set up and start consuming messages."""
    settings.QUEUE.consume_messages(
        settings.REPORTER_EXCHANGE,
        os.environ['REPORTER_ROUTING_KEYS'].split(),
        process_message,
        queue_name=os.environ.get('REPORTER_QUEUE')
    )


if __name__ == '__main__':
    main()
